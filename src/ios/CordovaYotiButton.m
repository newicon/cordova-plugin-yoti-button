#include "CordovaYotiButton.h"

@implementation CordovaYotiButton
@synthesize webView;

- (void)getButton:(CDVInvokedUrlCommand *)command
{

    UIButton *myButton = [UIButton buttonWithType:UIButtonTypeSystem];
    [myButton setFrame:CGRectMake(50, 200, 300, 50)];
    [myButton setTitle:@"My button" forState:UIControlStateNormal];
    [myButton setBackgroundColor:[UIColor blueColor]];
    [myButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];

    [self.viewController.view addSubview:myButton];

    [self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_OK] callbackId:command.callbackId];

}

@end
