#import <Cordova/CDVPlugin.h>
#import <UIKit/UIKit.h>

@interface CordovaYotiButton : CDVPlugin

- (void)getButton:(CDVInvokedUrlCommand*)command;

@end
