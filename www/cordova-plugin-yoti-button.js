var exec = require('cordova/exec');

var CordovaYotiButton = function () {};

CordovaYotiButton.prototype.getButton = function ({ callbackUrl, useCaseId, clientSdkId, scenarioId }, onSuccess, onError) {
    exec(onSuccess, onError, "CordovaYotiButton", "getButton", [
        callbackUrl,
        useCaseId,
        clientSdkId,
        scenarioId
    ]);
};

var yotibutton = new CordovaYotiButton();
module.exports = yotibutton;